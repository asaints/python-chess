from chessboard_classes import *

class Player:
	def __init__(self, playername, side):
		self.playername = playername
		self.side = side
		self.capturedpieces = []
		self.ownpieces = []
		self.playernext = None
		# self.playerturn = False

	def playerturn(self, chessboard):
		# self.playerturn = True;
		move = self.inputMove(chessboard)

		self.movepiece(chessboard, move)

		# self.playerturn = False;

	def inputMove(self, chessboard = None):
		piecestring = self.playername + "'s turn. \nSelect piece to move: "
		piecealgebcoordinates = input(piecestring)

		sourceTile = chessboard.getTile(piecealgebcoordinates)
		sourcepiece = sourceTile.occupant

		sourcepiece.generateMoveset(chessboard)

		locationstring = "Input piece destinaton: "
		location = input(locationstring)

		return [sourceTile, location]

	def movepiece(self, chessboard, move):
		oldTile = chessboard.getTile(move[0].name)
		newTile = chessboard.getTile(move[1])

		newTile.occupant = oldTile.occupant
		newTile.occupant.currenTile = newTile
		
		oldTile.occupant = None;

