from pieces_classes import *
from player_class import *
from chessboard_classes import *

def main():
	player1 = Player('Player 1', 'white')
	player2 = Player('Player 2', 'black')

	player1.playernext = player2
	player2.playernext = player1

	chessboard = Chessboard(player1, player2)



main()