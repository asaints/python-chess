Practice Python implementation of a CLI chess game.
 
 Currently implemented:
 - Chessboard, Chessboard Tile, Player, Piece, Chess-Piece classes
 - Initialization of chessboard
 - Initialization of game
 - Input of moves
 - Moveset generation for some Chess-Piece
 
 A work-in-progress.
 
 Python version: 3.7