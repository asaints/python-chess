import movesetgenerator_module as movgen

# Parent Piece Class
class Piece:
	def __init__(self, name, side):
		self.name = name
		self.side = side
		self.display = (name.upper() if side == "white" else name.lower())[0:3]
		self.moveset = []
		self.currentTile = None
		self.movesetRules = []

	def defineMovesetRules(self):
		print('Setting piece moveset rules.');

	def generateMoveset(self):
		print("Generating piece moveset.")


# Piece Sub-classes
class King(Piece):
	def __init__(self, side):
		Piece.__init__(self, "King", side)
		self.defineMovesetRules()

	def defineMovesetRules(self):
		self.movesetRules = ['straight-1-4', 'diagonal-1-4']

class Queen(Piece):
	def __init__(self, side):
		Piece.__init__(self, "Queen", side)
		self.defineMovesetRules()

	def defineMovesetRules(self):
		self.movesetRules = ['straight-n-4', 'diagonal-n-4']

class Bishop(Piece):
	def __init__(self, side):
		Piece.__init__(self, "Bishop", side)
		self.defineMovesetRules()

	def defineMovesetRules(self):
		self.movesetRules = ['diagonal-n-4']

	def generateMoveset(self, chessboard):
		print("Generating Bishop Piece moveset.")

		print(self.currentTile.display())
		print(self.movesetRules)

		self.moveset = movgen.generateMoves(chessboard, self);
		print(self.moveset)


class Knight(Piece):
	def __init__(self, side):
		Piece.__init__(self, "Knight", side)
		self.defineMovesetRules()

	def defineMovesetRules(self):
		self.movesetRules = ['Lshape-long-cw', 'Lshape-long-ccw' 'Lshape-short-cw', 'Lshape-short-ccw']

class Tower(Piece):
	def __init__(self, side):
		Piece.__init__(self, "Tower", side)
		self.defineMovesetRules()

	def defineMovesetRules(self):
		self.movesetRules = ['straight-n-4']

	def generateMoveset(self, chessboard):
		print("Generating Tower Piece moveset.")

		print(self.currentTile.display())
		print(self.movesetRules)

		self.moveset = movgen.generateMoves(chessboard, self);
		print(self.moveset)


class Pawn(Piece):
	def __init__(self, side):
		Piece.__init__(self, "Pawn", side)
		self.defineMovesetRules()

	def defineMovesetRules(self):
		self.movesetRules = ['straight-1-1', 'straight-2-1']

	def generateMoveset(self, chessboard):
		print("Generating Pawn Piece moveset.")

		print(self.currentTile.display())
		print(self.movesetRules)

		self.moveset = movgen.generateMoves(chessboard, self);
		print(self.moveset)