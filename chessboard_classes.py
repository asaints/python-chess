from pieces_classes import *
import string

class Tile:
	def __init__(self, name, coordinates):
		self.name = name
		self.coordinates = coordinates
		self.occupant = None

	def updateOccupant(self, piece):
		self.occupant = piece
		self.occupant.currentTile = self

	def display(self):
		tiledict = {
			"name": self.name,
			"coordinates": self.coordinates,
			"occupant": self.occupant.side + " " + self.occupant.name if self.occupant != None else None
		}

		return tiledict


class Chessboard:
	def __init__(self, player1, player2):
		self.player1 = player1
		self.player2 = player2
		self.chessboard = []

		self.setupboard()
		self.initializePieces()
		self.printboard()

		self.playgame();

	def setupboard(self):
		'''
		Coordinates (array indeces)
		[7, 0][7, 1][7, 2][7, 3][7, 4][7, 5][7, 6][7, 7]
		[6, 0][6, 1][6, 2][6, 3][6, 4][6, 5][6, 6][6, 7]
		[5, 0][5, 1][5, 2][5, 3][5, 4][5, 5][5, 6][5, 7]
		[4, 0][4, 1][4, 2][4, 3][4, 4][4, 5][4, 6][4, 7]
		[3, 0][3, 1][3, 2][3, 3][3, 4][3, 5][3, 6][3, 7]
		[2, 0][2, 1][2, 2][2, 3][2, 4][2, 5][2, 6][2, 7]
		[1, 0][1, 1][1, 2][1, 3][1, 4][1, 5][1, 6][1, 7]
		[0, 0][0, 1][0, 2][0, 3][0, 4][0, 5][0, 6][0, 7]

		Tile Names
		a8 b8 c8 d8 e8 f8 g8 h8
		a7 b7 c7 d7 e7 f7 g7 h7
		a6 b6 c6 d6 e6 f6 g6 h6
		a5 b5 c5 d5 e5 f5 g5 h5
		a4 b4 c4 d4 e4 f4 g4 h4
		a3 b3 c3 d3 e3 f3 g3 h3
		a2 b2 c2 d2 e2 f2 g2 h2
		a1 b1 c1 d1 e1 f1 g1 h1
		'''

		mapping = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
		for row in range(8):

			temprow = [];
			for column in range(8):
				tilename = mapping[column] + str(row + 1)
				coordinates = [row, column]
				newtile = Tile(tilename, coordinates)

				temprow.append(newtile)

			self.chessboard.append(temprow)

	def initializePieces(self):
		#initializing pawns both sides
		for column in range(0, 8):
			self.chessboard[6][column].updateOccupant(Pawn('black'))
			self.chessboard[1][column].updateOccupant(Pawn('white'))

		#initilizing black pieces
		self.chessboard[7][0].updateOccupant(Tower('black'))
		self.chessboard[7][7].updateOccupant(Tower('black'))
		self.chessboard[7][1].updateOccupant(Knight('black'))
		self.chessboard[7][6].updateOccupant(Knight('black'))
		self.chessboard[7][2].updateOccupant(Bishop('black'))
		self.chessboard[7][5].updateOccupant(Bishop('black'))
		self.chessboard[7][4].updateOccupant(King('black'))
		self.chessboard[7][3].updateOccupant(Queen('black'))

		self.chessboard[0][0].updateOccupant(Tower('white'))
		self.chessboard[0][7].updateOccupant(Tower('white'))
		self.chessboard[0][1].updateOccupant(Knight('white'))
		self.chessboard[0][6].updateOccupant(Knight('white'))
		self.chessboard[0][2].updateOccupant(Bishop('white'))
		self.chessboard[0][5].updateOccupant(Bishop('white'))
		self.chessboard[0][3].updateOccupant(King('white'))
		self.chessboard[0][4].updateOccupant(Queen('white'))

	def getTile(self, tilename):
		mapping = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

		tile = self.chessboard[int(tilename[1]) - 1][mapping.index(tilename[0])];
		return tile

	def printboard(self):
		print()
		print()
		columns = list(string.ascii_lowercase[:8])

		rowheaderstring = "     "
		for column in columns:
			rowheaderstring += "  " + column + "  "

		print(rowheaderstring)
		print()

		for row in range(7, -1, -1):
			temprowstring = "  " + str(row + 1) + "  ";
			for column in range(8):
				temprowstring += " " + (self.chessboard[row][column].name + " " if self.chessboard[row][column].occupant == None else self.chessboard[row][column].occupant.display) + " "

				# temprowstring += " " + (" . " if self.chessboard[row][column].occupant == None else self.chessboard[row][column].occupant.display) + " "
				# temprowstring += self.chessboard[row][column].name + " "
				# temprowstring += "[" + str(self.chessboard[row][column].coordinates[0]) + ", " + str(self.chessboard[row][column].coordinates[1]) + "]"
			temprowstring += "  " + str(row + 1) + "  ";
			print()

			print(temprowstring)

		print()
		print()
		print(rowheaderstring)

		print()
		print()

	def playgame(self):
		endGame = False;
		# self.player1.playerturn = True

		currentplayer = self.player1


		while endGame == False:
			currentplayer.playerturn(self);
			self.printboard()

			print()
			currentplayer = currentplayer.playernext




