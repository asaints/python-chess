import re

def straight(Board, piece, m, n):
	'''
	m is tiles to consider
	n is directions to consider
	chessboard is the chessboard
	'''
	basetile = piece.currentTile
	m = m if m != "n" else 8

	directions = ['upward', 'right', 'downward', 'left']

	if(piece.name == "Pawn"):
		mapping = {
			"white": ['upward', 'right', 'downward', 'left'],
			"black": ['downward', 'left', 'upward', 'right']
		}

		directions = mapping[piece.side]

	#m is tiles to consider
	#n is directions to consider

	possibletiles = []

	for counter in range(0, int(n)):
		if directions[counter] == "upward":
			try:
				nexttile = basetile.coordinates[0] + 1
				diff = 1

				while Board.chessboard[nexttile][basetile.coordinates[1]].occupant == None and diff <= int(m):
					if Board.chessboard[nexttile][basetile.coordinates[1]].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttile][basetile.coordinates[1]].name)

					nexttile += 1
					diff += 1

			except IndexError:
				pass


		elif directions[counter] == "right":
			try:
				nexttile = basetile.coordinates[1] + 1
				diff = 1

				while Board.chessboard[basetile.coordinates[0]][nexttile].occupant == None and diff <= int(m):
					if Board.chessboard[basetile.coordinates[0]][nexttile].name not in possibletiles:
						possibletiles.append(Board.chessboard[basetile.coordinates[0]][nexttile].name)

					nexttile += 1
					diff += 1

			except IndexError:
				pass

		elif directions[counter] == "downward":
			try:
				nexttile = basetile.coordinates[0] - 1
				diff = 1

				while Board.chessboard[nexttile][basetile.coordinates[1]].occupant == None and diff <= int(m):
					if Board.chessboard[nexttile][basetile.coordinates[1]].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttile][basetile.coordinates[1]].name)

					nexttile -= 1
					diff += 1

			except IndexError:
				pass

		elif directions[counter] == "left":
			try:
				nexttile = basetile.coordinates[1] - 1
				diff = 1

				while Board.chessboard[basetile.coordinates[0]][nexttile].occupant == None and diff <= int(m):
					if Board.chessboard[basetile.coordinates[0]][nexttile].name not in possibletiles:
						possibletiles.append(Board.chessboard[basetile.coordinates[0]][nexttile].name)

					nexttile -= 1
					diff += 1

			except IndexError:
				pass

	return possibletiles

def diagonal(Board, piece, m, n):
	'''
	m is tiles to consider
	n is directions to consider
	chessboard is the chessboard
	'''
	basetile = piece.currentTile
	m = m if m != "n" else 8
	n = n if n != "n" else 8

	directions = ['topleft', 'topright', 'botright', 'botleft']

	#m is tiles to consider
	#n is directions to consider

	possibletiles = []

	for counter in range(0, int(n)):
		if directions[counter] == "topleft":
			try:
				nexttileX = basetile.coordinates[1] - 1
				nexttileY = basetile.coordinates[0] + 1
				diff = 1

				while Board.chessboard[nexttileY][nexttileX].occupant == None and diff <= int(m):
					if Board.chessboard[nexttileY][nexttileX].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttileY][nexttileX].name)

					nexttileY += 1
					nexttileX -= 1
					diff += 1

			except IndexError:
				pass


		elif directions[counter] == "topright":
			try:
				nexttileX = basetile.coordinates[1] + 1
				nexttileY = basetile.coordinates[0] + 1
				diff = 1

				while Board.chessboard[nexttileY][nexttileX].occupant == None and diff <= int(m):
					if Board.chessboard[nexttileY][nexttileX].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttileY][nexttileX].name)

					nexttileY += 1
					nexttileX += 1
					diff += 1

			except IndexError:
				pass

		elif directions[counter] == "botright":
			try:
				nexttileX = basetile.coordinates[1] + 1
				nexttileY = basetile.coordinates[0] - 1
				diff = 1

				while Board.chessboard[nexttileY][nexttileX].occupant == None and diff <= int(m):
					if Board.chessboard[nexttileY][nexttileX].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttileY][nexttileX].name)

					nexttileY -= 1
					nexttileX += 1
					diff += 1

			except IndexError:
				pass

		elif directions[counter] == "botleft":
			try:
				nexttileX = basetile.coordinates[1] - 1
				nexttileY = basetile.coordinates[0] - 1
				diff = 1

				while Board.chessboard[nexttileY][nexttileX].occupant == None and diff <= int(m):
					if Board.chessboard[nexttileY][nexttileX].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttileY][nexttileX].name)

					nexttileY -= 1
					nexttileX -= 1
					diff += 1

			except IndexError:
				pass

	return possibletiles

def lshape(Board, piece, m, n):
	'''
	m is [long, short]
	n is [clockwise, counterclockwise]
	chessboard is the chessboard
	'''
	basetile = piece.currentTile
	m = m if m != "n" else 8
	n = n if n != "n" else 8

	if n == clockwise:
		directions = ['upward', 'right', 'bottom', 'left']

	else:
		directions = ['upward', 'left', 'bottom', 'right']

	

	#m is tiles to consider
	#n is directions to consider

	possibletiles = []

	lengthmapping = {
		'long': {
			"y": 2,
			"x": 1
		},

		'short': {
			"y": 1,
			"x": 2
		}
	}

	for counter in range(0, len(directions)):
		if directions[counter] == "upward":
			try:
				nexttileX = basetile.coordinates[1] - 1
				nexttileY = basetile.coordinates[0] + 1
				diff = 1

				while Board.chessboard[nexttileY][nexttileX].occupant == None and diff <= int(m):
					if Board.chessboard[nexttileY][nexttileX].name not in possibletiles:
						possibletiles.append(Board.chessboard[nexttileY][nexttileX].name)

					nexttileY += 1
					nexttileX -= 1
					diff += 1

			except IndexError:
				pass


	return possibletiles






def generateMoves(chessboard, piece):
	moveset = []
	for rule in piece.movesetRules:
		if(re.search("straight", rule) != None):
			splitparams = rule.split('-')

			moveset = moveset + list(set(straight(chessboard, piece, splitparams[1], splitparams[2])) - set(moveset));
			# print(moveset)

		elif(re.search("diagonal", rule) != None):
			splitparams = rule.split('-')

			moveset = moveset + list(set(diagonal(chessboard, piece, splitparams[1], splitparams[2])) - set(moveset));

		elif(re.search("Lshape", rule) != None):
			splitparams = rule.split('-')

	return moveset


		